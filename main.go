package main

import (
	"fmt"
	"github.com/gen2brain/beeep"
	"github.com/pborman/getopt/v2"
	"gitlab.com/Alvoras/ringo_watcher/internal/cli"
	"gitlab.com/Alvoras/ringo_watcher/internal/config"
	log "gitlab.com/Alvoras/ringo_watcher/internal/loggers"
	"gitlab.com/Alvoras/ringo_watcher/internal/site"
	"gitlab.com/Alvoras/ringo_watcher/internal/utils"
	gomail "gopkg.in/gomail.v2"
	"io/ioutil"
	golog "log"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var (
	// Config is used to receive the json mapping of the config file
	cfg config.Config
	// *args is populated with the parsed command line arguments
	args cli.Args

	sites         site.Sites
	gnomeNotifCmd string
	notifyAgent   func(string, string, string) error
	dialogAgent   func(string, string, string) error
)

func init() {
	gnomeNotifCmd = "zenity"

	log.Event = golog.New(os.Stdout, "[+] ", 0)
	log.Info = golog.New(os.Stdout, "[>] ", 0)
	log.Error = golog.New(os.Stdout, "[-] ", 0)

	portLimit := &getopt.UnsignedLimit{
		Base: 10,
		Bits: 64,
		Min:  1,
		Max:  65534,
	}

	args.URL = getopt.StringLong("url", 'u', "", "The url to watch for availability")
	args.Port = getopt.UnsignedLong("port", 'p', 0, portLimit, "The port on which to send requests")
	args.Mail = getopt.StringLong("mail", 'm', "", "The email address to which the mail notification will be sent when the site is available")
	args.QueryInterval = getopt.DurationLong("query-interval", 'i', 10*time.Second, "Request interval duration. Default is 10 seconds.\nValid duration units are \"ns\", \"us\" (or \"µs\"), \"ms\", \"s\", \"m\", \"h\".\nExample : -i 2s")
	args.Flags.Notify = getopt.BoolLong("notif", 'n', "Switch to display a notification when the watched site is accessible")
	args.Flags.Dialog = getopt.BoolLong("dialog", 'N', "Switch to display a dialog notification when the watched site is accessible")
	args.Flags.Help = getopt.BoolLong("help", 'h', "Print this help")
	args.Flags.Verbose = getopt.BoolLong("verbose", 'v', "Display additional informations")
	// Format url string
	getopt.Parse()

	if len(*args.URL) == 0 || *args.Flags.Help {
		getopt.PrintUsage(os.Stdout)
		return
	}

	if *args.Flags.Verbose {
		log.Verbose = golog.New(os.Stdout, "[>] ", 0)
	} else {
		log.Verbose = golog.New(ioutil.Discard, "[>] ", 0)
	}

	// -p only works in single-site mode
	if *args.Port != 0 && strings.Contains(*args.URL, ",") == false {
		*args.URL += ":" + strconv.FormatUint(*args.Port, 10)
	}

	cfg = config.LoadConfigFileJSON()

	notifyAgent = beeep.Notify
	dialogAgent = beeep.Alert

	if runtime.GOOS == "linux" {
		// Use zenity over beeep's library linux tools
		_, err := exec.LookPath(gnomeNotifCmd)
		if err == nil {
			notifyAgent = utils.Notify
			dialogAgent = utils.NotifyDialog
		}
	}

	// URLs are parsed and formated
	// Then we fetch the initial state for each of those
	// If requesting a url raise an error, we remove it from the list
	// Then the sites.List is populated with the remaining sites
	sites.Load(site.LoadURLs(*args.URL))
}

func main() {

	log.Info.Println("Waiting for status change...")

	for {
		querySites(sites.List)
		time.Sleep(*args.QueryInterval)
	}
}

func notifyChange(site site.Site) {
	log.Event.Println(fmt.Sprintf("Status has changed for %s", site.URL))
	log.Info.Println(fmt.Sprintf("\tReceived <%s> from %s", site.Status.Verbose, site.URL))
	log.Info.Println("\tLast status : ", site.Status.Last)
	log.Info.Println("\tCurrent status : ", site.Status.Current)

	if len(*args.Mail) != 0 {
		log.Event.Println("\tSending mail notification to", *args.Mail, "...")
		sendMail(*args.Mail, site.URL, site.Status.Last, site.Status.Current)
	}

	if !*args.Flags.Notify && !*args.Flags.Dialog {
		return
	}

	log.Event.Println("\tSending notification")
	if *args.Flags.Notify {
		go func() {
			err := notifyAgent(gnomeNotifCmd, site.URL, site.Status.Verbose)
			utils.HandleErr(err, false)
		}()
	}

	if *args.Flags.Dialog {
		go func() {
			err := dialogAgent(gnomeNotifCmd, site.URL, site.Status.Verbose)
			utils.HandleErr(err, false)
		}()

	}
}

func querySites(sites []site.Site) {
	// Pong receive channel
	rc := make(chan site.Pong)

	// Send concurrent HEAD request for each site
	for _, s := range sites {
		log.Verbose.Println(fmt.Sprintf("Sending a HEAD request to %s...", s.URL))

		go func(site site.Site, rc chan site.Pong) {
			pong := site.FetchStatus(site.URL)
			utils.HandleErr(pong.Err, false)
			rc <- pong
		}(s, rc)
	}

	// Wait to receive each response
	for i := 0; i < len(sites); i++ {
		res := <-rc

		// Set current status
		sites[res.ID].Status.Current = res.StatusCode
		sites[res.ID].Status.Verbose = res.Status

		if res.Err != nil {
			utils.HandleRequestError(res.Err, false)
			// os.Exit(1)
		}

		log.Verbose.Println(fmt.Sprintf("Received %s", sites[res.ID].Status.Verbose))
		statusChanged := sites[res.ID].Status.Last != sites[res.ID].Status.Current

		if statusChanged {
			notifyChange(sites[res.ID])
			// Update the last status with the current new status
			sites[res.ID].Status.Last = sites[res.ID].Status.Current
		}
	}

}

func updateStatus(url string) (int, string, error) {
	status := "Error while requesting the url"
	statusCode := 0
	res, err := http.Head(url)

	if res != nil {
		status = res.Status
		statusCode = res.StatusCode
	}

	return statusCode, status, err
}

func sendMail(dest string, url string, lastStatus int, currentStatus int) bool {
	m := gomail.NewMessage()

	subjectHeader := fmt.Sprintf("The site %s is accessible", url)
	body := fmt.Sprintf(
		`The status for the site %s has changed.<br>
				[+] Last status : <%d><br>
				[+] Current status : <%d>`,
		url, lastStatus, currentStatus)

	m.SetHeader("From", "ringo.notifier@gmail.com")
	m.SetHeader("To", dest)
	m.SetHeader("Subject", subjectHeader)
	m.SetBody("text/html", body)

	dialer := gomail.NewDialer(cfg.Mail.SMTP.Address, cfg.Mail.SMTP.Port, cfg.Mail.Account.Address, cfg.Mail.Account.Password)

	// Send the email to Bob, Cora and Dan.
	if err := dialer.DialAndSend(m); err != nil {
		log.Error.Println(err)
		return false
	}

	log.Event.Println(fmt.Sprintf("Mail sent to %s", dest))
	return true
}
