package cli

import (
	"time"
)

// Args is a struc holding all the cli args that the user can control
type Args struct {
	URL           *string
	Port          *uint64
	Mail          *string
	QueryInterval *time.Duration
	Flags         struct {
		Notify  *bool
		Dialog  *bool
		Help    *bool
		Verbose *bool
	}
}
