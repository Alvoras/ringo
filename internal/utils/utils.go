package utils

import (
	"fmt"
	log "gitlab.com/Alvoras/ringo_watcher/internal/loggers"
	"os"
	"os/exec"
	"strings"
)

func HandleErr(err error, doPanic bool) {
	if err != nil {
		if doPanic {
			panic(err)
		} else {
			log.Verbose.Println(err)
		}
	}
}

func HandleRequestError(err error, exit bool) {
	if err != nil {
		if exit {
			log.Error.Println(err)
			os.Exit(1)
		} else {
			errMsg := "Unknown error"
			if strings.Contains(err.Error(), "no such host") {
				errMsg = "There is nothing at this url"
			} else if strings.Contains(err.Error(), "connection refused") {
				errMsg = "Failed to connect to the url. Please check your internet connection"
			} else if strings.Contains(err.Error(), "certificate is valid for") {
				errMsg = "Failed to connect to the url. The HTTPS certificate appears to be invalid"
			}
			log.Error.Println(errMsg)
		}
	}
}

func NotifyDialog(path string, url string, httpStatus string) error {
	err := exec.Command("zenity", "--info", fmt.Sprintf("--title=Status for site %s has changed", url), fmt.Sprintf("--text=Response : %s", httpStatus)).Run()
	if err != nil {
		return err
	}

	return nil
}

func Notify(path string, url string, httpStatus string) error {
	err := exec.Command(path, "--notification", fmt.Sprintf("--text=Status for site %s has changed\nResponse : %s", url, httpStatus)).Run()
	if err != nil {
		return err
	}

	return nil
}
