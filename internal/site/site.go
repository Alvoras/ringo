package site

import (
	"fmt"
	log "gitlab.com/Alvoras/ringo_watcher/internal/loggers"
	"gitlab.com/Alvoras/ringo_watcher/internal/utils"
	"net/http"
	"strings"
)

type Pong struct {
	ID         int
	Status     string
	StatusCode int
	Err        error
}

type Site struct {
	URL string
	ID  int
	Status
}

type Status struct {
	Last    int
	Current int
	Verbose string
}

type Sites struct {
	List []Site
	// List map[string]Site
}

func (s *Sites) Remove(idx int) {
	s.List[len(s.List)-1], s.List[idx] = s.List[idx], s.List[len(s.List)-1]
	s.List = s.List[:len(s.List)-1]
}

func LoadURLs(rawURLs string) []Site {
	lastDefault := 0
	currentDefault := 0
	verboseDefault := ""

	var sites []Site

	urls := strings.Split(rawURLs, ",")
	for idx, url := range urls {
		s := Site{
			URL: url,
			ID:  idx,
		}
		s.Status = Status{
			Last:    lastDefault,
			Current: currentDefault,
			Verbose: verboseDefault,
		}

		sites = append(sites, s)
	}

	return sites
}

func (s *Sites) Load(URLList []Site) {
	rc := make(chan Pong)

	// Load the current state for each sites
	for _, s := range URLList {
		go func(site Site, rc chan Pong) {
			pong := site.FetchStatus(site.URL)
			utils.HandleErr(pong.Err, false)
			rc <- pong
		}(s, rc)

		log.Info.Println("Watching " + s.URL)
	}

	for i := 0; i < len(URLList); i++ {
		pong := <-rc

		if pong.Err != nil {
			utils.HandleRequestError(pong.Err, false)
			log.Event.Println(fmt.Sprintf("This site will not be tracked : %s", URLList[pong.ID].URL))
		} else {
			// Set current status
			URLList[pong.ID].ID = len(s.List)
			URLList[pong.ID].Status.Current = pong.StatusCode
			URLList[pong.ID].Status.Last = pong.StatusCode
			URLList[pong.ID].Status.Verbose = pong.Status

			// Load into the tracked sites list
			s.List = append(s.List, URLList[pong.ID])
			log.Info.Println(fmt.Sprintf("Current status for %s is %s", URLList[pong.ID].URL, pong.Status))
		}
	}
}

func (s *Site) FetchStatus(url string) Pong {
	var res *http.Response
	var p Pong
	p.Status = "Error while requesting the url"
	p.ID = s.ID
	p.StatusCode = 0
	res, p.Err = http.Head(url)

	if res != nil {
		p.Status = res.Status
		p.StatusCode = res.StatusCode
	}

	return p
}
