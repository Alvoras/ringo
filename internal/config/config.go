package config

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Alvoras/ringo_watcher/internal/utils"
	"io/ioutil"
	"os"
)

// Config structure which mirrors the json file
type Config struct {
	Mail struct {
		Account struct {
			Address  string `json:"address"`
			Password string `json:"password"`
		} `json:"account"`

		SMTP struct {
			Address string `json:"address"`
			Port    int    `json:"port"`
		} `json:"smtp"`
	} `json:"mail"`
}

func LoadConfigFileJSON() Config {
	var c Config
	configPath := "./config.json"
	cfgData, err := ioutil.ReadFile(configPath)
	utils.HandleErr(err, true)

	if err := json.Unmarshal(cfgData, &c); err != nil {
		fmt.Println(fmt.Sprintf("Failed to load the config file (%s)", configPath))
		fmt.Println(err)
		os.Exit(2)
	}

	return c
}
