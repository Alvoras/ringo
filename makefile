RINGO_RUN=go run ringo.go
DEFAULT_200_STATUS=https://httpstat.us/200
BASE_STATUS=https://httpstat.us

200:
	$(RINGO_RUN) -u $(DEFAULT_200_STATUS)

404:
	$(RINGO_RUN) -u $(BASE_STATUS)/404

503:
	$(RINGO_RUN) -u $(BASE_STATUS)/503

loop:
	$(RINGO_RUN) -u $(DEFAULT_200_STATUS) -ln

help:
	$(RINGO_RUN) --help

build:
	go build ringo.go

push:
	./gp.sh
